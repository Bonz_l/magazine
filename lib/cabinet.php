<?php
	if (!isset($_SERVER['PHP_AUTH_USER'])) {
		header('WWW-Authenticate: Basic realm="My Realm"');
		header('HTTP/1.0 401 Unauthorized');
	} else {
		$auth = User::auth($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']);
		if ($auth=="auth") {
			if (isset($_GET['create_book'])) {
				if ($_GET['create_book']=='add') {
					include "$public/../lib/create_book.php";
				} else {
					include "$public/../templates/create_book.html";
				}
			} else if (isset($_GET['order'])) {
				include "$public/../lib/order.php";
			} else {
				include "$public/../templates/cabinet.html";
			}
		} else {
			header('WWW-Authenticate: Basic realm="My Realm"');
			header('HTTP/1.0 401 Unauthorized');
		}
	}

