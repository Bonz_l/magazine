<?php

$book_id = Db::fetch("SELECT object_id FROM urls WHERE url_crc=crc32('$url')");
if (!$book_id) {
	echo "Ошибка чтения в urls";
	exit;
}
$cart = Cart::dataCart($session);
$book = Db::fetchAll("SELECT * FROM books WHERE book_id =".(int)$book_id["object_id"]);
if (!$book) {
	echo "Ошибка чтения в book";
	exit;
}
$smarty->assign('book', $book);
$smarty->assign('cart', $cart);

$smarty->display("$public/../templates/book.html");
