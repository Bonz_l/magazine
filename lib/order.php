<?php
$sql = Db::fetchAll("SELECT * FROM client");

$ar = [];
foreach ($sql as $value) {
	$sum = 0;
	$sess_id= $value['session_id'];
	$prod = Db::fetchAll("SELECT order.price, books.name,  COUNT(prod_id) as col
	 FROM `order` JOIN books
	 ON order.prod_id=books.book_id
	 WHERE session_id = $sess_id
	 GROUP BY prod_id");

	foreach ($prod as $key=>$val) {
		if ((int) $val['col']>1) {
			$prod[$key]['total_prod'] = (float)$val['col'] * (float)$val['price'];
		} else {
			$prod[$key]['total_prod'] = (float)$val['price'];
		}
	}

	$price = Db::fetchAll("SELECT price from `order` WHERE session_id=$sess_id");

	foreach ($price as $val) {
		$sum += (float) $val['price'];
	}

	$value['prod'] = $prod;
	$value['total'] = $sum;

	array_push($ar, $value);
}

$smarty->assign('order', $ar);
$smarty->display("$public/../templates/order.html");
