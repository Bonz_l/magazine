<?php
$public = $_SERVER['DOCUMENT_ROOT'];

require_once("$public/../smarty/Smarty.class.php");
$smarty = new Smarty();
$smarty->template_dir = "$public/../templates/";
$smarty->compile_dir = "$public/../templates_c/";
$smarty->config_dir = "$public/../configs/";
$smarty->cache_dir = "$public/../cache/";

require_once ("Db.php");
require_once ("Cart.php");
require_once ("User.php");

require_once("$public/../configs.php");
