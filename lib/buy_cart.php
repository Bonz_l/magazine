<?php
if (isset($_GET['buy'])) {
	if (isset($_POST['book_id']) && isset($_POST['price'])) {
		$id = $_POST['book_id'];
		$price = (float) $_POST['price'];
		$amount = Db::fetch("SELECT amount FROM cart WHERE prod_id=$id AND session_id=$session");

		if (!empty($amount)) {
			$new_col = (int)$amount["amount"] + 1;
			Db::query("UPDATE cart SET amount=$new_col WHERE prod_id=$id AND session_id=$session");
		} else {
			$sql = Db::query("INSERT INTO cart (session_id, prod_id, price, amount) VALUES ('$session', '$id', '$price', '1')");
			if (!$sql) {
				echo "Ошибка записи в cart";
				exit;
			}
		}
		$cartData = Cart::dataCart($session);
		echo json_encode($cartData);
	}
} else if (isset($_GET['del']) || isset($_GET['del_all'])) {
	if (isset($_POST['del_prod'])) {
		$id = (float) $_POST['del_prod'];
		$sql = Db::query("DELETE FROM cart WHERE session_id=$session AND prod_id=$id");
		if (!$sql) {
			echo "Ошибка удаления в cart";
			exit;
		}
	} else if (isset($_POST['del_all'])) {
		$sql = Db::query("DELETE FROM cart WHERE session_id=$session");
		if (!$sql) {
			echo "Ошибка удаления всех товаров";
			exit;
		}
	}
	header('Location: http://magazine.local/cart');
} else if (isset($_GET['re-count'])) {
	if (isset($_POST['prods'])) {
		$prods = json_decode($_POST['prods'], true);
		foreach ($prods as $key=>$val) {
			$prod_id = $val["prod_id"];
			$count = Db::fetch("SELECT amount FROM cart WHERE prod_id=$prod_id AND session_id=$session");
			if ((int)$count['amount'] != $val["amount"]) {
				$sql = Db::query("UPDATE cart SET amount=".$val['amount']." WHERE prod_id=".$val["prod_id"]." AND session_id=$session");
				if (!$sql) {
					echo "Ошибка записи amount";
					exit;
				}
			}
		}
	}
	header('Location: http://magazine.local/cart');
} else if (isset($_GET['order']))  {
	if (isset($_POST['name']) && $_POST['phone'] ) {
		$name = Db::escapeString($_POST['name']);
		$phone = Db::escapeString($_POST['phone']);

		$sql = Db::query("INSERT INTO `order`(session_id, prod_id, price) SELECT session_id, prod_id, price FROM cart WHERE cart.session_id=$session");
		if (!$sql) {
			echo "Ошибка записи в order";
			exit;
		}

		$sql = Db::query("INSERT INTO `client`(session_id, name, phone) VALUES ('$session', '$name', '$phone')");
		if (!$sql) {
			echo "Ошибка записи в client";
			exit;
		}

		$sql = Db::query("DELETE FROM `cart` WHERE session_id=$session");
		if (!$sql) {
			echo "Ошибка удаления cart";
			exit;
		}
		header('Location: http://magazine.local/cart?order=done');
	}
} else {
	if (isset($_GET['order']) && $_GET['order'] == 'done') {
		$smarty->assign('order', 'done');
	} else {
		$cart = Db::fetchAll("SELECT cart.price, cart.prod_id, cart.amount, books.name
		FROM cart
		JOIN books ON cart.prod_id=books.book_id
		WHERE cart.session_id=$session");
		if (!isset($cart)) {
			echo "Ошибка запроса в cart";
			exit;
		}
		$cart_total = Cart::dataCart($session);
		if (isset($cart)) {
			foreach ($cart as $key=>$value) {
				if ((int) $value['amount']>1) {
					$cart[$key]['total_prod'] = (float)$value['amount'] * (float)$value['price'];
				} else {
					$cart[$key]['total_prod'] = (float)$value['price'];
				}
			}
			if ($cart_total['amount']==0) {
				$smarty->assign('order', 'none');
			} else {
				$smarty->assign('order', 'yes');
			}
			$smarty->assign('cart', $cart);
			$smarty->assign('cart_total', $cart_total);
		}
	}
	$smarty->display("$public/../templates/cart.html");
}