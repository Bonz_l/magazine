<?php
$prod = Db::fetchAll('SELECT books.name, books.price, books.book_id, urls.url FROM books JOIN urls ON book_id=object_id');
$cart = Cart::dataCart($session);
if (isset($prod) || isset($cart)) {
	$smarty->assign('books', $prod);
	$smarty->assign('cart', $cart);
}
$smarty->display("$public/../templates/main.html");