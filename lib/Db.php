<?php
class Db {
	private static $_connection;
	private static $_db;

	private static function _connnect() {
		self::$_connection = mysqli_connect(MYSQLI_HOST, MYSQLI_USER, MYSQLI_PASS);

		if (!self::$_connection) {
			die("No MySQL Connection");
		}

		self::$_db = mysqli_select_db(self::$_connection, MYSQLI_DB);

		if (!self::$_db) {
			die("No database ".MYSQLI_DB." exists");
		}
	}

	static function escapeString($str) {
		self::_connnect();
		return mysqli_real_escape_string(self::$_connection, $str);
	}

	static function query($sql) {
		self::_connnect();
		return mysqli_query(self::$_connection, $sql);
	}

	static function fetch($sql) {
		$res = self::query($sql);
		if (!$res) return false;
		return mysqli_fetch_assoc($res);
	}

	static function fetchAll($sql) {
		$res = self::query($sql);
		if (!$res) return false;
		return mysqli_fetch_all($res, MYSQLI_ASSOC);
	}

}