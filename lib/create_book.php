<?php

if (isset($_REQUEST['name']) && isset($_REQUEST['price'])) {

	$name = Db::escapeString($_REQUEST['name']);
	$price = Db::escapeString((float)$_REQUEST['price']);
	$desc = Db::escapeString($_REQUEST['desc']);

	var_dump($desc);

	function rus2translit($string) {
		$converter = array(
			'а' => 'a',   'б' => 'b',   'в' => 'v',
			'г' => 'g',   'д' => 'd',   'е' => 'e',
			'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
			'и' => 'i',   'й' => 'y',   'к' => 'k',
			'л' => 'l',   'м' => 'm',   'н' => 'n',
			'о' => 'o',   'п' => 'p',   'р' => 'r',
			'с' => 's',   'т' => 't',   'у' => 'u',
			'ф' => 'f',   'х' => 'h',   'ц' => 'c',
			'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
			'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
			'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

			'А' => 'A',   'Б' => 'B',   'В' => 'V',
			'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
			'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
			'И' => 'I',   'Й' => 'Y',   'К' => 'K',
			'Л' => 'L',   'М' => 'M',   'Н' => 'N',
			'О' => 'O',   'П' => 'P',   'Р' => 'R',
			'С' => 'S',   'Т' => 'T',   'У' => 'U',
			'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
			'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
			'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
			'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
		);
		return strtr($string, $converter);
	}

	function str2url($str) {
		$str = rus2translit($str);
		$str = strtolower($str);
		$str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
		$str = trim($str, "-");
		return $str;
	};

	$url = str2url($_REQUEST['name']);

	$sql = Db::query("INSERT INTO books (`name`, `price`, `desc`) VALUES ('$name', '$price', '$desc')");
	if (!$sql) {
		echo "Ошибка записи в books";
		exit;
	}

	$lastId = Db::fetch("SELECT book_id FROM books ORDER BY book_id DESC LIMIT 1");
	if (!$lastId) {
		echo "Ошибка чтения books";
		exit;
	}
	$url = "/".$url;
	$crc = crc32("/".$url);

	$lstid = $lastId['book_id'];

	$sql = Db::query("INSERT INTO urls (object_id, type_id, url, url_crc) VALUES ('$lstid', '3', '$url', '$crc')");
	if (!$sql) {
		echo "Ошибка записи в urls";
		exit;
	}

}

header('Location: http://magazine.local/user?create_book');