<?php
class Cart {
	static function dataCart($session) {
		$data = [];

		$sql = Db::fetch("SELECT SUM(amount) as amount, SUM(amount*price) as total FROM cart WHERE session_id=$session");
		if (!$sql) {
			echo "Ошибка получение всех товаров в корзине";
			exit;
		}

		if ($sql['amount'] == NULL) {
			$data['amount'] = 0;
			$data['total'] = 0;
		} else {
			$data['amount'] = $sql['amount'];
			$data['total'] = $sql['total'];
		}

		return $data;
	}
}