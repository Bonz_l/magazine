<?php
include '../lib/bootstrap.php';

session_start();
$url = $_SERVER["DOCUMENT_URI"];
$session = crc32(session_id());

if ($url == "/") {
	include "$public/../lib/main.php";
} else {
	$object = Db::fetch("SELECT * FROM urls WHERE url_crc=".crc32(Db::escapeString($url)));
	if (!$object) {
		include "$public/../templates/404.html";
	} else {
		switch ($object['type_id']) {
			case '1':
				include "$public/../lib/buy_cart.php";
				break;
			case '2':
				include "$public/../lib/cabinet.php";
				break;
			case '3':
				include "$public/../lib/book.php";
				break;
			default:
				include "../views/404.php";
				break;
		}
	}
}