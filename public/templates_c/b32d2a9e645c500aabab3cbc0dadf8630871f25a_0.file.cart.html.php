<?php
/* Smarty version 3.1.28, created on 2016-07-28 09:52:25
  from "/home/yura-pc/magazine/templates/cart.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_57998f8950b852_74344302',
  'file_dependency' => 
  array (
    'b32d2a9e645c500aabab3cbc0dadf8630871f25a' => 
    array (
      0 => '/home/yura-pc/magazine/templates/cart.html',
      1 => 1469681540,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../templates/header.tpl' => 1,
    'file:../templates/bottom.tpl' => 1,
  ),
),false)) {
function content_57998f8950b852_74344302 ($_smarty_tpl) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div class="cart_prods">
	<?php if (isset($_smarty_tpl->tpl_vars['order']->value) && $_smarty_tpl->tpl_vars['order']->value == 'done') {?>
	ваш заказ оформлен
	<?php } else { ?>
	<?php
$_from = $_smarty_tpl->tpl_vars['cart']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_e_0_saved_item = isset($_smarty_tpl->tpl_vars['e']) ? $_smarty_tpl->tpl_vars['e'] : false;
$_smarty_tpl->tpl_vars['e'] = new Smarty_Variable();
$__foreach_e_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_e_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['e']->value) {
$__foreach_e_0_saved_local_item = $_smarty_tpl->tpl_vars['e'];
?>
	<div class="prod" style="margin-bottom:20px;">
		<div>Название: <?php echo $_smarty_tpl->tpl_vars['e']->value['name'];?>
</div>
		<div>Цена: <?php echo $_smarty_tpl->tpl_vars['e']->value['price'];?>
</div>
		<div>Кол-во: <input class="amount" style="width:30px; text-align:center" type="text" data-prod_id="<?php echo $_smarty_tpl->tpl_vars['e']->value['prod_id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['e']->value['amount'];?>
"></div>
		<div>Сумма: <?php echo $_smarty_tpl->tpl_vars['e']->value['total_prod'];?>
</div>
		<form action="/cart?del" method="post">
			<input type="hidden"  value="<?php echo $_smarty_tpl->tpl_vars['e']->value['prod_id'];?>
" name="del_prod">
			<input type="submit" value="Удалить">
		</form>

	</div>
	<?php
$_smarty_tpl->tpl_vars['e'] = $__foreach_e_0_saved_local_item;
}
}
if ($__foreach_e_0_saved_item) {
$_smarty_tpl->tpl_vars['e'] = $__foreach_e_0_saved_item;
}
?>
		<?php if (isset($_smarty_tpl->tpl_vars['order']->value) && $_smarty_tpl->tpl_vars['order']->value != 'none') {?>
		<form action="/cart?re-count" method="post" class="re-count">
			<input type="hidden"  value="" name="prods">
			<input type="submit" value="Пересчитать">
		</form>
		<div>На сумму: <?php echo $_smarty_tpl->tpl_vars['cart_total']->value['total'];?>
</div>
		<form action="/cart?del_all" method="post">
			<input type="hidden"  value="all" name="del_all">
			<input type="submit" value="Очистить корзину">
		</form>

		<form action="/cart?order" method="post">
			<input name="name" value="" placeholder="Name"> <br />
			<input name="phone" value="" placeholder="Телефон"> <br />
			<input type="submit" value="Оформить заказ">
		</form>
		<?php } else { ?>
		Корзина пуста
		<?php }?>
	<?php }?>

</div>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../templates/bottom.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
