<?php
/* Smarty version 3.1.28, created on 2016-07-28 14:34:20
  from "/home/yura-pc/magazine/templates/header.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5799d19c2ab359_82374167',
  'file_dependency' => 
  array (
    '90a5c69f7f589f2ba1dbb906245815beaaf65380' => 
    array (
      0 => '/home/yura-pc/magazine/templates/header.tpl',
      1 => 1469698458,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5799d19c2ab359_82374167 ($_smarty_tpl) {
?>
<html>
<head>
	<meta charset="utf-8">
	<title>Магазин</title>
	<?php echo '<script'; ?>
 type="text/javascript" src="../js/jquery-1.11.2.min.js"><?php echo '</script'; ?>
>
	<link href="../css/style.css" type="text/css" rel="stylesheet">
	<?php echo '<script'; ?>
>
		var cart_recount = [];
		$(function() {

			$('.buy').click(function(){
				var prod_id = $(this).closest('.prod').find('input[name=book_id]').val();
				var price = $(this).closest('.prod').find('input[name=price]').val();
				$.ajax({
					url: "/cart?buy",
					type: "post",
					data: "book_id="+prod_id+"&price="+price,
					success: function (response) {
						console.log(response);
						try {
							var data = JSON.parse(response);
						} catch (e) {
							alert('Что то пошло не так' +response );
						}
						$('.cart .amount span').text(data.amount);
						$('.cart .total span').text(data.total);
					}
				});
			});
			$('.amount').keyup(function(){
				var val = parseInt($(this).val());
				var prods_wrap = $(this).closest('.cart_prods').find('form.re-count');
				if (!isNaN(val)) {

					if (!prods_wrap.is('.show')) {

						prods_wrap.show();
					}
					var prod = {};
					var prod_id = $(this).data("prod_id");
					prod.prod_id = prod_id;
					prod.amount = val;
					if(cart_recount.length>0) {
						var exist_t = false;
						for (var i = 0; i < cart_recount.length; i++) {
							if (cart_recount[i]['prod_id'] == prod['prod_id'] ) {
								exist_t = true;
								if (cart_recount[i]['amount'] != prod['amount']) {
									cart_recount[i]['amount'] = prod['amount'];
									break;
								}
							}
						}
						if (exist_t == false) {
							cart_recount.push(prod);
						}
					} else {
						cart_recount.push(prod);
					}
					prods_wrap.find('input[name=prods]').val(JSON.stringify(cart_recount));
				} else {
					alert('Введите число');
				}
			});
			
		})
	<?php echo '</script'; ?>
>
</head>
<body>
<div style="padding: 0 0 20px"><a href="/">Перейти на главную</a></div>
<?php }
}
