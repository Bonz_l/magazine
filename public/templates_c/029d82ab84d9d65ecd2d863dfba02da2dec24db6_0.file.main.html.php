<?php
/* Smarty version 3.1.28, created on 2016-07-28 14:10:14
  from "/home/yura-pc/magazine/templates/main.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5799cbf62a40a1_27999583',
  'file_dependency' => 
  array (
    '029d82ab84d9d65ecd2d863dfba02da2dec24db6' => 
    array (
      0 => '/home/yura-pc/magazine/templates/main.html',
      1 => 1469697013,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../templates/header.tpl' => 1,
    'file:../templates/bottom.tpl' => 1,
  ),
),false)) {
function content_5799cbf62a40a1_27999583 ($_smarty_tpl) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<div class="cart">
	Корзина
	<div class="amount">Кол-во <span><?php echo $_smarty_tpl->tpl_vars['cart']->value['amount'];?>
</span></div>
	<div class="total">Сумма <span><?php echo $_smarty_tpl->tpl_vars['cart']->value['total'];?>
</span></div>
	<a href="/cart">Перейти в корзину</a>
</div>

<?php
$_from = $_smarty_tpl->tpl_vars['books']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_e_0_saved_item = isset($_smarty_tpl->tpl_vars['e']) ? $_smarty_tpl->tpl_vars['e'] : false;
$_smarty_tpl->tpl_vars['e'] = new Smarty_Variable();
$__foreach_e_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_e_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['e']->value) {
$__foreach_e_0_saved_local_item = $_smarty_tpl->tpl_vars['e'];
?>
<div class="prod">
	<div><strong>Название:</strong> <?php echo $_smarty_tpl->tpl_vars['e']->value['name'];?>
</div>
	<div><strong>Цена:</strong> <?php echo $_smarty_tpl->tpl_vars['e']->value['price'];?>
</div>
	<div><a href="<?php echo $_smarty_tpl->tpl_vars['e']->value['url'];?>
">Подробнее</a></div>
	<input type="hidden" name="book_id" value="<?php echo $_smarty_tpl->tpl_vars['e']->value['book_id'];?>
">
	<input type="hidden" name="price" value="<?php echo $_smarty_tpl->tpl_vars['e']->value['price'];?>
">
	<div class="buy">Купить</div>
</div>
<?php
$_smarty_tpl->tpl_vars['e'] = $__foreach_e_0_saved_local_item;
}
}
if ($__foreach_e_0_saved_item) {
$_smarty_tpl->tpl_vars['e'] = $__foreach_e_0_saved_item;
}
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:../templates/bottom.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>



<?php }
}
